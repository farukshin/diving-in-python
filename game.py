import random

number = random.randint(0, 101)

while True:
    res = input("Введите число:")
    if not res or res == "exit":
        break

    if not res.isdigit():
        print("Введите правильное число!")
        continue

    user_num = int(res)

    if user_num < number:
        print("Загаданное число больше")
    elif user_num > number:
        print("Загаданное число меньше")
    else:
        print("Угадали!")
        break
    