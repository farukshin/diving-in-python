import os
import tempfile
import argparse
import json

parser = argparse.ArgumentParser()
parser.add_argument("--key")
parser.add_argument("--val")
args = parser.parse_args()

storage_path = os.path.join(tempfile.gettempdir(), 'storage.data')
if args.key and args.val:
    try:
        with open(storage_path, 'r') as f:
            load_store = json.loads(f.read())
        if not args.key in load_store:
            load_store[args.key] = []
        load_store[args.key].append(args.val)
        with open(storage_path, 'w') as f:
            f.write(json.dumps(load_store))    
    except FileNotFoundError:
        kv_store = {}
        kv_store[args.key] = [args.val]
        with open(storage_path, 'w') as f:
            f.write(json.dumps(kv_store))

elif args.key:
    try:
        with open(storage_path, 'r') as f:
            load_store = json.loads(f.read())
        if args.key in load_store:
            print(', '.join(load_store[args.key]))
        else:
            print('')
    except FileNotFoundError:   
        pass
else:
    print('')