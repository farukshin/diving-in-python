from functools import reduce

print(list(map(lambda x: str(x), range(10))))

print(reduce(lambda a,b: a*b, [1, 2, 3, 4, 5]))

square_list1 = [number **2 for number in range(10)]
square_list2 = [number for number in range(10) if number % 2 == 0]
print(square_list2)